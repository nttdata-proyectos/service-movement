package com.nttdata.movementservice.infraestructure.data.service.client.rest;


import com.nttdata.movementservice.domain.document.AccountDto;
import com.nttdata.movementservice.domain.interfaces.AccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

public class AccountServiceRest implements AccountService {
  private final WebClient webClient;
  private final WebClient webClientAccount;

  public AccountServiceRest(WebClient.Builder webClientBuilder, WebClient webClientAccount) {
    this.webClient = webClientBuilder.baseUrl("http://localhost:8885/").build();
    this.webClientAccount = webClientBuilder.baseUrl("http://localhost:8887/").build();
  }

  @Override
  public Mono<AccountDto> findById(String id) {
    return this.webClient.get().uri("/list/" + id)
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .onStatus(HttpStatus::is4xxClientError,
            error -> Mono.error(new RuntimeException("Verificar id de la Cuenta")))
        .bodyToMono(AccountDto.class);
  }

  @Override
  public Mono<AccountDto> updateAccountAmount(String accountId, double amount) {
    return this.webClientAccount.get().uri("account/amount/" + accountId + "/" + amount)
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .onStatus(HttpStatus::is4xxClientError,
            error -> Mono.error(new RuntimeException("Verifique el monto de la Cuenta")))
        .bodyToMono(AccountDto.class);
  }

  @Override
  public Mono<AccountDto> findByAccountNumber(String accountNumber) {
    return webClientAccount.get().uri("account/account-number/" + accountNumber)
        .accept(MediaType.APPLICATION_JSON)
        .retrieve()
        .onStatus(HttpStatus::is4xxClientError,
            error -> Mono.error(new RuntimeException("Fallo en Recuperar Cuenta")))
        .bodyToMono(AccountDto.class);
  }

}
