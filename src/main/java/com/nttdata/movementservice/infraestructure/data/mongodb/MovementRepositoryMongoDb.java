package com.nttdata.movementservice.infraestructure.data.mongodb;

import com.nttdata.movementservice.infraestructure.data.document.Movement;
import java.time.LocalDate;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface MovementRepositoryMongoDb extends ReactiveMongoRepository<Movement,String> {
  Flux<Movement> findByRegisterDateBetween(LocalDate from, LocalDate to);
}
