package com.nttdata.movementservice.infraestructure.data.document;

import com.nttdata.movementservice.domain.document.AccountDto;
import com.nttdata.movementservice.domain.document.TransactionType;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "movements")
public class Movement {
    private String id;
    private String transactionCode;
    private TransactionType transactionType;
    private Double amount;
    private Double transactionFee; //tarifa de Transaccion
    private Double totalAmount;
    private AccountDto account;
    private LocalDateTime registerDate;

}