package com.nttdata.movementservice.utils;

import com.nttdata.movementservice.domain.document.MovementDto;
import com.nttdata.movementservice.infraestructure.data.document.Movement;
import org.springframework.beans.BeanUtils;

public class Convert {

    public static MovementDto entityToDTO(Movement movement) {
        MovementDto movementDTO = new MovementDto();
        BeanUtils.copyProperties(movement, movementDTO);
        return movementDTO;

    }
    public static Movement DtoToEntity(MovementDto movementDto){
        Movement movement=new Movement();
        BeanUtils.copyProperties(movementDto,movement);
        return movement;
    }





}
