package com.nttdata.movementservice.domain.services;

import static com.nttdata.movementservice.utils.MessageUtils.getMsg;

import com.nttdata.movementservice.domain.document.MovementDto;
import com.nttdata.movementservice.domain.document.TransactionType;
import com.nttdata.movementservice.domain.interfaces.AccountService;
import com.nttdata.movementservice.domain.interfaces.MovementService;
import com.nttdata.movementservice.infraestructure.data.mongodb.MovementRepositoryMongoDb;
import com.nttdata.movementservice.utils.Convert;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import javax.security.auth.login.AccountNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.retry.Retry;

@Service
public class MovementServiceImpl implements MovementService {

  @Autowired
  MovementRepositoryMongoDb movRepositoryDb;

  @Autowired
  AccountService accountServiceWebClient;

  @Override
  public Flux<MovementDto> findAll() {
    return movRepositoryDb.findAll().map(Convert::entityToDTO);
  }

  @Override
  public Mono<MovementDto> findById(String id) {
    return movRepositoryDb.findById(id).map(Convert::entityToDTO);
  }

  @Override
  public Mono<MovementDto> save(Mono<MovementDto> move) {
    return null;
  }

  @Override
  public Mono<Void> delete(String id) {
    return null;
  }

  @Override
  public Flux<MovementDto> findByAccountId(String accountId) {
    return null;
  }

  @Override
  public Flux<MovementDto> findByAccountNumber(String accountNumber) {
    return null;
  }
  @Override
  public Mono<MovementDto> deposit(MovementDto transactionDto) {
    return Mono.just(transactionDto)
        .flatMap(this::existAccount)
        .flatMap(dto -> this.transaction(dto, TransactionType.DEPOSIT))
        .subscribeOn(Schedulers.boundedElastic());
  }
  @Override
  public Mono<MovementDto> withdraw(MovementDto transactionDto) {
    return Mono.just(transactionDto)
        .flatMap(this::existAccount)
        .flatMap(dto -> this.transaction(dto, TransactionType.WITHDRAWAL))
        .subscribeOn(Schedulers.boundedElastic());
  }

  private Mono<MovementDto> existAccount(MovementDto movementDto) {
    return accountServiceWebClient.findByAccountNumber(movementDto.getAccountNumber())
        .switchIfEmpty(Mono.error(new AccountNotFoundException(getMsg("account.not.found"))))
        .doOnNext(movementDto::setAccount)
        .retryWhen(Retry.fixedDelay(3, Duration.ofSeconds(1)))
        .thenReturn(movementDto);
  }


  private Mono<MovementDto> transaction(MovementDto transactionDto,
                                        TransactionType transactionType) {
    return Mono.just(transactionDto)
        .map(Convert::DtoToEntity)
        .map(entity -> {

          if (entity.getTransactionFee() == null) {
            entity.setTransactionFee(0D);
            entity.setTotalAmount(entity.getAmount());
          }

          entity.setRegisterDate(LocalDateTime.now());
          entity.setTransactionCode(generateRandomNumber(8));
          entity.setTransactionType(transactionType);
          return entity;
        })
        .flatMap(movRepositoryDb::save)
        .map(Convert::entityToDTO)
        .flatMap(this::updateAccountAmount);
  }

  private Mono<MovementDto> updateAccountAmount(MovementDto transactionDto) {
    final double amount = transactionDto.getTransactionType().isDeposit()
        ? transactionDto.getAmount()
        : transactionDto.getAmount() * -1;
    return accountServiceWebClient.updateAccountAmount(transactionDto.getAccount().getId(), amount)
        .doOnNext(transactionDto::setAccount)
        .thenReturn(transactionDto);
  }
  public static String generateRandomNumber(int length) {
    Random random = new Random(System.currentTimeMillis());
    String bin = "8678";
    int randomNumberLength = length - (bin.length() + 1);
    var builder = new StringBuilder(bin);
    for (int i = 0; i < randomNumberLength; i++) {
      int digit = random.nextInt(10);
      builder.append(digit);
    }

    int checkDigit = getCheckDigit(builder.toString());
    builder.append(checkDigit);
    return builder.toString();
  }
  private static int getCheckDigit(String number) {
    int sum = 0;
    for (int i = 0; i < number.length(); i++) {

      // Obtenga el dígito en la posición actual.
      int digit = Integer.parseInt(number.substring(i, (i + 1)));

      if ((i % 2) == 0) {
        digit = digit * 2;
        if (digit > 9) {
          digit = (digit / 10) + (digit % 10);
        }
      }
      sum += digit;
    }

    // El dígito de control es el número requerido para que la suma sea un múltiplo de 10

    int mod = sum % 10;
    return ((mod == 0) ? 0 : 10 - mod);
  }

  @Override
  public Flux<MovementDto>  findByDateRange(String from, String to) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    LocalDate fromDate = LocalDate.parse(from, formatter);
    LocalDate toDate = LocalDate.parse(to, formatter);
    return movRepositoryDb.findByRegisterDateBetween(fromDate, toDate)
        .map(Convert::entityToDTO);
  }

}
