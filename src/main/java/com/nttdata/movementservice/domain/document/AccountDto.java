package com.nttdata.movementservice.domain.document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountDto {
    private String id;
    private String type;  //tipo de Cuenta(Personal o Empresarial)
    private Double amount;
    private String commission;
    private List<String> permissions;  //firmantes autorizados(uno o muchos)
    private List<String> idClients;   //muchos clientes en caso de cuenta Empresarial || 1 Personal
    private Integer limit;
    private Date createAt;
}
