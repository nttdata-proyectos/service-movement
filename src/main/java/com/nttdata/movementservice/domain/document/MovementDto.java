package com.nttdata.movementservice.domain.document;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovementDto {
    private String id;
    @NotBlank(message = "accountNumber is required")
    @JsonProperty(access = WRITE_ONLY)
    private String accountNumber;
    private String transactionCode;
    private TransactionType transactionType;
    private Double amount;
    private Double transactionFee;
    private Double totalAmount;
    private AccountDto account;
    private LocalDateTime registerDate;
}

