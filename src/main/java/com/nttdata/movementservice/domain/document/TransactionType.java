package com.nttdata.movementservice.domain.document;

public enum TransactionType {
  DEPOSIT, WITHDRAWAL;
  public boolean isDeposit() {
    return this.equals(DEPOSIT);
  }
}
