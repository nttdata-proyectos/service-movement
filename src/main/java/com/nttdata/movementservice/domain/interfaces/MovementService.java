package com.nttdata.movementservice.domain.interfaces;

import com.nttdata.movementservice.domain.document.MovementDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MovementService {

    public Flux<MovementDto> findAll();
    public Mono<MovementDto> findById(String id);
    public Mono<MovementDto> save(Mono<MovementDto> move);
    public Mono<Void> delete(String id);
    Flux<MovementDto> findByAccountId(String accountId);

    Flux<MovementDto> findByAccountNumber(String accountNumber);

    Mono<MovementDto> deposit(MovementDto transactionDto);

    Mono<MovementDto> withdraw(MovementDto transactionDto);

    Flux<MovementDto> findByDateRange(String from, String to);

}
