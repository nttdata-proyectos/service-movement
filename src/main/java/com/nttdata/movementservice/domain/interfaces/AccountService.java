package com.nttdata.movementservice.domain.interfaces;

import com.nttdata.movementservice.domain.document.AccountDto;
import reactor.core.publisher.Mono;

public interface AccountService {

    public Mono<AccountDto> findById(String id);

    public Mono<AccountDto> updateAccountAmount(String accountId, double amount);
    public Mono<AccountDto>  findByAccountNumber(String accountNumber);

}
