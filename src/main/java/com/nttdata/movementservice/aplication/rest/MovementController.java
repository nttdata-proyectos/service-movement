package com.nttdata.movementservice.aplication.rest;

import com.nttdata.movementservice.domain.document.MovementDto;
import com.nttdata.movementservice.domain.interfaces.AccountService;
import com.nttdata.movementservice.domain.interfaces.MovementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/movement")
public class MovementController {

    public String idAccount;
    public String typeAccount;
    private static final Logger log = LoggerFactory.getLogger(MovementController.class);
    public static final String CUENTA_CORRIENTE = "CUENTA CORRIENTE";
    public static final String CUENTAAHORROS = "CUENTA AHORROS";
    public static final String CUENTAPLAZOFIJO = "CUENTA PLAZO FIJO";
    @Autowired
    MovementService movementService;
    @Autowired
    AccountService accountServiceRest;


    @GetMapping
    public String index() {
        return "okMovement";
    }

    @PostMapping
    public Mono<MovementDto> register(@RequestBody Mono<MovementDto> movementDTO) {
        return movementService.save(movementDTO);
    }
    @GetMapping("/list")
    public Flux<MovementDto> findAll() {
        return movementService.findAll();
    }
    @GetMapping("/delete/{id}")
    public Mono<Void> delete(@PathVariable String id) {
        return movementService.delete(id);
    }

    // se realizo el movimiento para CUENTA DE AHORRO

}
